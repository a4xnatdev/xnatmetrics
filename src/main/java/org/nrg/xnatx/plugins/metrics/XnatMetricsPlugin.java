package org.nrg.xnatx.plugins.metrics;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@XnatPlugin(name = "XNAT Metrics Plugin", value = "xnat-metrics", entityPackages = "org.nrg.xnatx.plugins.metrics.entities", logConfigurationFile = "metrics-logback.xml")
@ComponentScan({ "org.nrg.xnatx.plugins.metrics.preferences", "org.nrg.xnatx.plugins.metrics.rest",
				 "org.nrg.xnatx.plugins.metrics.services", "org.nrg.xnatx.plugins.metrics.services.impl",
				 "org.nrg.xnatx.plugins.metrics.daos","org.nrg.xnatx.plugins.metrics.component",
				 "org.nrg.xnatx.plugins.metrics.providers","org.nrg.xnatx.plugins.metrics.providers.impl"})
@Slf4j
public class XnatMetricsPlugin {

    /**
     * Creates XnatMetricsPlugin instance.
     */
    public XnatMetricsPlugin() {
    	log.info("Creating XnatMetrics Plugin configuration class");
    }

    /**
     * Instantiates {@link RestTemplate restTemplate} bean.
     */
    @Bean
    public RestTemplate restTemplate() {
    	return new RestTemplate();
    }

}

package org.nrg.xnatx.plugins.metrics.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MetricsException extends Exception {

	public MetricsException(String exception) {
		super(exception);
		log.error(this.getMessage());
	}

}

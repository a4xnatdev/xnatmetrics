package org.nrg.xnatx.plugins.metrics.preferences;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;

import lombok.extern.slf4j.Slf4j;

import org.nrg.framework.configuration.ConfigPaths;
import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.beans.AbstractPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.services.NrgPreferenceService;
import org.nrg.xnatx.plugins.metrics.XnatMetricsPlugin;
import org.nrg.xnatx.plugins.metrics.exception.MetricsException;
import org.nrg.xnatx.plugins.metrics.utils.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Manages preference settings for the {@link XnatMetricsPlugin XNAT metrics
 * plugin} and its various services.
 */
@NrgPreferenceBean(toolId = "xnat-metrics", toolName = "Metrics Plugin")
@Slf4j
public class MetricsPreferencesBean extends AbstractPreferenceBean {
    public static final String PREF_ENABLED = "xnatMetricsEnabled";
    public static final String PREF_SITE_UUID = "siteUuid";
    public static final String PREF_PI_NAME = "piName";
    public static final String PREF_PI_EMAIL = "piEmail";
    public static final String PREF_ADMIN_NAME = "adminName";
    public static final String PREF_SCHEDULER_ENABLED = "schedulerEnabled";
    public static final String PREF_SUBMIT_INTERVAL = "submitInterval";
    
    /**
     * Creates a new instance of the metrics preference bean.
     *
     * @param preferenceService The site preference service.
     * @param configFolderPaths Contains paths for the locations of configuration
     *                          folders on the system.
     */
    @Autowired
    public MetricsPreferencesBean(final NrgPreferenceService preferenceService, final ConfigPaths configFolderPaths) {
	super(preferenceService, configFolderPaths);

    }

    /**
     * Gets the site UUID.
     *
     * @return The site UUID.
     */
    @NrgPreference(defaultValue = "Unknown")
    public String getSiteUuid() {
	return getValue(PREF_SITE_UUID);
    }

    /**
     * Sets the site UUID.
     *
     * @param siteUuid The UUID to set for the site.
     */
    public void setSiteUuid(final String siteUuid) {
	try {
	    set(siteUuid, PREF_SITE_UUID);
	} catch (InvalidPreferenceName e) {
	    log.error("Invalid preference name siteUuid: something is very wrong here.", e);
	}
    }

    /**
     * Gets the primary investigator's name.
     *
     * @return The primary investigator's name.
     */
    @NrgPreference(defaultValue = "Unknown")
    public String getPiName() {
	return getValue(PREF_PI_NAME);
    }

    /**
     * Sets the primary investigator's name.
     *
     * @param piName The name to set for the primary investigator.
     */
    public void setPiName(final String piName) {
	try {
	    set(piName, PREF_PI_NAME);
	} catch (InvalidPreferenceName e) {
	    log.error("Invalid preference name piName: something is very wrong here.", e);
	}
    }

    /**
     * Gets the primary investigator's email.
     *
     * @return The primary investigator's email.
     */
    @NrgPreference(defaultValue = "Unknown")
    public String getPiEmail() {
	return getValue(PREF_PI_EMAIL);
    }

    /**
     * Sets the primary investigator's email.
     *
     * @param piEmail The email to set for the primary investigator.
     */
    public void setPiEmail(final String piEmail) {
	try {
	    ValidationUtils.matchEmail(piEmail);
	    set(piEmail, PREF_PI_EMAIL);
	} catch (InvalidPreferenceName e) {
	    log.error("Invalid preference name piEmail: something is very wrong here.", e);
	} catch (MetricsException e) {
	    log.error("Email format is not valid", e);
	}
    }

    /**
     * Gets the primary administrator's name.
     *
     * @return The primary administrator's name.
     */
    @NrgPreference(defaultValue = "Unknown")
    public String getAdminName() {
	return getValue(PREF_ADMIN_NAME);
    }

    /**
     * Sets the primary administrator's name.
     *
     * @param adminName The name to set for the primary administrator.
     */
    public void setAdminName(final String adminName) {
	try {
	    set(adminName, PREF_ADMIN_NAME);
	} catch (InvalidPreferenceName e) {
	    log.error("Invalid preference name adminName: something is very wrong here.", e);
	}
    }

    /**
     * Gets the submit Interval for the Scheduler.
     *
     * @return {@link String String} submit Interval
     */
    @NrgPreference(defaultValue = "weekly") // Weekly
    public String getSubmitInterval() {
    	return getValue(PREF_SUBMIT_INTERVAL);
    }

    /**
     * Sets the submit Interval for the Scheduler.
     *
     * @param submitInterval
     */
    public void setSubmitInterval(final String submitInterval) {
	try {
	    set(submitInterval,PREF_SUBMIT_INTERVAL);
	} catch (InvalidPreferenceName e) {
	    log.error("Invalid preference name submitInterval: something is very wrong here.", e);
	}
    }

    /**
     * Gets the schedulerEnabled flag.
     *
     * @return Boolean schedulerEnabled flag.
     */
    @NrgPreference(defaultValue = "false") 
    public Boolean getSchedulerEnabled() {
	return getBooleanValue(PREF_SCHEDULER_ENABLED);
    }

    /**
     * Sets the scheduler Enabled flag.
     *
     * @param Boolean schedulerEnabled.
     */
    public void setSchedulerEnabled(final Boolean schedulerEnabled) {
	try {
	    setBooleanValue(schedulerEnabled, PREF_SCHEDULER_ENABLED);
	} catch (InvalidPreferenceName e) {
	    log.error("Invalid preference name submitInterval: something is very wrong here.", e);
	}
    }

}

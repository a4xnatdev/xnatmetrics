package org.nrg.xnatx.plugins.metrics.dto;

import org.nrg.xnatx.plugins.metrics.preferences.MetricsPreferencesBean;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * DTO for collecting report.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(prefix = "_")
@Builder(builderClassName = "Builder")
@JsonPropertyOrder({ MetricsPreferencesBean.PREF_SITE_UUID, MetricsPreferencesBean.PREF_PI_NAME,
	MetricsPreferencesBean.PREF_PI_EMAIL, MetricsPreferencesBean.PREF_ADMIN_NAME, "siteId", "siteUrl", "adminEmail",
	"projectCount", "subjectCount", "sessionCount", "scanCount", "siteAdminCount", "enabledUserCount",
	"investigatorCount" })
public class ReportData {

    private String _siteUuid;
    private String _siteId;
    private String _siteUrl;
    private String _piName;
    private String _piEmail;
    private String _adminName;
    private String _adminEmail;
    private long _projectCount;
    private long _subjectCount;
    private long _sessionCount;
    private long _scanCount;
    private long _siteAdminCount;
    private long _enabledUserCount;
    private long _investigatorCount;
}

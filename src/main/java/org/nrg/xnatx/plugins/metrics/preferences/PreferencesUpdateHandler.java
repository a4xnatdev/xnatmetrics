package org.nrg.xnatx.plugins.metrics.preferences;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotNull;

import lombok.extern.slf4j.Slf4j;

import org.nrg.xdat.security.user.XnatUserProvider;
import org.nrg.xnat.event.listeners.methods.AbstractScheduledXnatPreferenceHandlerMethod;
import org.nrg.xnat.task.AbstractXnatRunnable;
import org.nrg.xnatx.plugins.metrics.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

@Component
@Slf4j
//@Getter(PROTECTED)
//@Setter(PRIVATE)
//@Accessors(prefix = "_")
public class PreferencesUpdateHandler extends AbstractScheduledXnatPreferenceHandlerMethod {

    @Autowired
    public PreferencesUpdateHandler(final ThreadPoolTaskScheduler scheduler,
	    final XnatUserProvider primaryAdminUserProvider, final ReportService service,
	    final MetricsPreferencesBean preferences) {
		super(scheduler, primaryAdminUserProvider, INTERVAL);
		_service = service;
		_preferences = preferences;
		_interval.put("weekly","0 0/2 * * * *");
		_interval.put("monthly", "0 0/5 * * * *");
		System.out.println("Service instantiated.");
    }

    @NotNull
    protected AbstractXnatRunnable getTask() {
	return new AbstractXnatRunnable() {
	    @Override
	    protected void runTask() {
		try {
			if(_preferences.getSchedulerEnabled()){
				_service.generateReport();
				System.out.println("Report created and sent by Scheduler. " + sdf.format(new Date()));
			}
		} catch (Exception e) {
		    log.error("An error occurred trying to submit a report by Scheduler", e);
		}
	    }
	};
    }

    @NotNull
    protected Trigger getTrigger() {
    	System.out.print("Interval set to "+_interval.get(getSubmitInterval()));
    	return new CronTrigger(_interval.get(getSubmitInterval()));
    }

    private String getSubmitInterval() {
    	return _preferences.getSubmitInterval();
    }

    /**
     * Updates the value for the specified preference according to the preference
     * type.
     *
     * @param preference The preference to set.
     * @param value      The value to set.
     */
    protected void handlePreferenceImpl(final String preference, final String value) {
		log.debug("Found preference {} that this handler can handle, setting value to {}", preference, value);
		setSubmitInterval(value);
    }

    private void setSubmitInterval(String value) {
    	_preferences.setSubmitInterval(value);
    }

    private static final String INTERVAL = "submitInterval";

    private final ReportService _service;

    private String _submitInterval;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

    private final MetricsPreferencesBean _preferences;
    
    private final Map<String,String> _interval = new HashMap<String,String>();
}

package org.nrg.xnatx.plugins.metrics.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.net.URL;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(prefix = "_")
@Builder(builderClassName = "Builder")
@JsonPropertyOrder({"version","projects","subjects","experiments","dataTypes","experimentsByDataTypes","users","platformInfo","plugins"})
public class CoreXnatMetrics implements Serializable{
	private String _version;
	private URL _siteUrl;
	private String _uuid;
	private long _projects;
	private long _subjects;
	private long _experiments;
	private long _dataTypes;
	private Map<String,Integer> _experimentsByDataType;
	private long _users;
	private String _platformInfo;
	private Map<String,String> _plugins;
}

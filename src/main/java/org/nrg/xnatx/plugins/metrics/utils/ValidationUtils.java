package org.nrg.xnatx.plugins.metrics.utils;

import lombok.extern.slf4j.Slf4j;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xnatx.plugins.metrics.exception.MetricsException;
import org.nrg.xnatx.plugins.metrics.preferences.MetricsPreferencesBean;

/**
 * Provides convenience and utility methods for common validations.
 */
@Slf4j
public class ValidationUtils {
    public static Pattern DATE_REGEX = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");
    public static Pattern EMAIL_REGEX = Pattern.compile("^[A-Za-z0-9+_.-]+@(.+)$");
    public static List<String> PREFERENCES = Arrays.asList(MetricsPreferencesBean.PREF_PI_NAME,
	    MetricsPreferencesBean.PREF_PI_EMAIL);
    public static List<String> SCHEDULER_PREFERENCES = Arrays.asList(MetricsPreferencesBean.PREF_SCHEDULER_ENABLED,
	    MetricsPreferencesBean.PREF_SUBMIT_INTERVAL);
    public static List<String> SCHEDULER_CRON_STRING = Arrays.asList("0 0 2 ? * MON", "0 0 2 1 * ?", "0 * * * * *",
	    "0 */2 * * * *");

    /**
     * Validates the date using the {@link #DATE_REGEX} regular expression.
     *
     * @param date The date to be validated.
     *
     * @throws MetricsException When the date doesn't match the {@link #DATE_REGEX}
     *                          regular expression.
     */
    @SuppressWarnings("unused")
    public static void matchDate(final String date) throws MetricsException {
	if (!DATE_REGEX.matcher(date).matches()) {
	    throw new MetricsException("Invalid date preference format");
	}
    }

    /**
     * Validates the email address using the {@link #EMAIL_REGEX} regular
     * expression.
     *
     * @param email The email address to be validated.
     *
     * @throws MetricsException When the email address doesn't match the
     *                          {@link #EMAIL_REGEX} regular expression.
     */
    public static void matchEmail(final String email) throws MetricsException {
	if (!EMAIL_REGEX.matcher(email).matches()) {
	    throw new MetricsException("Invalid email preference format");
	}
    }

    /**
     * Checks that the map of preferences contains all of the preferences specified
     * in the {@link #PREFERENCES} list and none that aren't in the list.
     *
     * @param preferences The map of preferences and values.
     *
     * @throws MetricsException When the preferences don't match the names specified
     *                          in {@link #PREFERENCES}.
     */
    public static int checkPreferenceMap(final Map<String, Object> preferences) throws MetricsException {
	Boolean checkPreferencesFlag = true, checkSchedulerPreferencesFlag = true;
	int identifier = 0; // Identifier to be returned : 1 for Preferences and 2 for SchedulerPreferences;
	if (preferences.size() != 2) {
	    log.error("The preferences are not valid. Must include all of: "
		    + StringUtils.join(PREFERENCES, ", ") + " or " + StringUtils.join(SCHEDULER_PREFERENCES, ","));
	    return 0;
	}
	for (final String preference : PREFERENCES) {
	    if (!preferences.containsKey(preference)) {
		checkPreferencesFlag = false;
		break;
	    }
	}
	for (final String schedulerPreference : SCHEDULER_PREFERENCES) {
	    if (!preferences.containsKey(schedulerPreference)) {
		checkSchedulerPreferencesFlag = false;
		break;
	    }
	}
	if (checkPreferencesFlag) {
	    identifier = 1;
	} else if (checkSchedulerPreferencesFlag) {
	    identifier = 2;
	}
	return identifier;
    }
}

package org.nrg.xnatx.plugins.metrics.dto;

import java.util.List;

import lombok.Builder;
import lombok.Data;

/**
 * DTO layer for Pharos Collector Service.
 */
@Data
@Builder(builderClassName = "Builder")
public class PharosReportDto {
    private String uuid;
    private String version;
    private String ipAddress;
    private List<Object> reportData;
    
}

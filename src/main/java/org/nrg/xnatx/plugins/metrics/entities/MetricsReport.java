package org.nrg.xnatx.plugins.metrics.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

@Entity
@Access(AccessType.FIELD)
@Data
@Accessors(prefix = "_")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonPropertyOrder({ "status", "statusText", "attempts", "report", "lastAttempt", "version" })
//@JsonIgnoreProperties("report")
@TypeDefs({ @TypeDef(name = "json", typeClass = JsonStringType.class),
	@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class) })
public class MetricsReport extends AbstractHibernateEntity {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Min(0)
    @Max(599)
    private int _status;

    @Size(min = 1, max = 100)
    private String _statusText;

    private int _attempts;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private List<Object> _report;

    private Date _lastAttempt;

    private String _version;
}

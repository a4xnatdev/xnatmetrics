/*
 * web: investigators.js
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2017, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

/*!
 * Functions for creating and modifying projects
 */

console.log('GenerateReport.js',XNAT);

var XNAT = getObject(XNAT);

(function(factory){
    if (typeof define === 'function' && define.amd) {
        define(factory);
    }
    else if (typeof exports === 'object') {
        module.exports = factory();
    }
    else {
        return factory();
    }

    }(function(){

        var $metDataSection = $$('div#metadata-div');

        var $scheduleSection = $$('div#schedule-div')

        var $manager = $$('div#generate-report');

        // Meta Data tab
        function metaDataFunction() {
            var metaDataVar = {
                metaData: {
                    kind: "panel.form",
                    name: 'metaData',
                    label: "Metadata",
                    method: "POST",
                    url: "/xapi/metrics/prefs/",
                    contentType: "json",
                    contents: {
                        siteUuid: {
                            kind: "panel.input.text",
                            name: "siteUuid",
                            label: "Site UUID",
                            placeholder: "Site UUID",
                            validations: "onblur",
                            element: {
                                disabled : true
                            }
                        },
                        piName: {
                            kind: "panel.input.text",
                            name: "piName",
                            label: "Primary Investigator Name",
                            placeholder: "PI Name",
                            validations: "onblur"
                        },
                        piEmail: {
                            kind: "panel.input.email",
                            name: "piEmail",
                            label: "Primary Investigator Email",
                            placeholder: "PI Email",
                            validations: "onblur"
                        },
                        adminName: {
                            kind: "panel.input.text",
                            name: "adminName",
                            label: "Admin Name",
                            placeholder: "admin name",
                            validations: "onblur",
                            element: {
                                disabled : true
                            }
                        },
                    },
                }
            }
            XNAT.spawner.spawn(metaDataVar).render($metDataSection);
        }

        // Schedule tab
        function scheduleFunction() {
            var scheduleVar = {
                schedule: {
                    kind: "panel.form",
                    name: 'schedule',
                    label: "Schedule",
                    method: "POST",
                    url: "/xapi/metrics/prefs/",
                    contentType: "json",
                    contents: {
                        schedulerEnabled: {
                            kind: "panel.input.switchbox",
                            name: "schedulerEnabled",
                            label: "Report Scheduler",
                            id: "schedulerEnabled",
                            onText: "Enable",
                            offText: "Disable",
                            element:{
                            	style: "margin: 0 0 5px 0"
                            }
                        },
                        submitInterval: {
                            kind: "panel.element",
                            label: "Report Frequency",
                            contents: {
                                infoLine: {
                                    tag: "p",
                                    element: {
                                        style: "margin: 0 0 5px 0",
                                    },
                                    content: "Select report frequency"
                                },
                                reportFreqWeekly: {
                                    kind: "input.radio",
                                    name: "submitInterval",
                                    id: "reportFreqWeekly",
                                    value: "weekly",
                                    after: "<label class='pad5h' for='reportFreqWeekly'>Weekly</label>"
                                },
                                reportFreqMonthly: {
                                    kind: "input.radio",
                                    name: "submitInterval",
                                    id: "reportFreqMonthly",
                                    value: "monthly",
                                    after: "<label class='pad5h' for='reportFreqMonthly'>Monthly</label>"
                                }, 
                            }
                        }
                    } 
                }
            }
            XNAT.spawner.spawn(scheduleVar).render($scheduleSection);
            console.log("SCHEDULER VAR======",scheduleVar)
        }

        // Create report Table
        var reportTable = XNAT.table.dataTable([], {
            id: 'itemTable',
            load: '/xapi/metrics/reports',

            id: 'itemTable',
            load: '/xapi/metrics/reports',
            columns: {
                id: {
                    label: 'ID',
                    filter: true,
                    className: 'center'
                },
                created: {
                    label: 'Created',
                    filter: true,
                    apply: function(){
                        var item = this;
                        var date = new Date(item.created)
                        var reportDate = date.toLocaleString();
                        return spawn('div.center', reportDate + '');
                    },
                },
                status: {
                    label: 'Status',
                    filter: true,
                    className: 'center'
                },
                statusText: {
                    label: 'Status Text',
                    filter: true,
                    className: 'center'
                },
                attempts: {
                    label: 'Attempts',
                    filter: true,
                    className: 'center'
                },
                text: {
                    label: 'Text',
                    value: "&ndash;",
                    className: 'center',
                    html: '<a href="#!" class="view-report-info link nowrap" >View JSON</a>'
                }
            },
        }).render('div#report-table-container');

        // View JSON modal popup
        $(document).on('click', 'a.view-report-info', function(e){

            e.preventDefault();

            var _id  = $(this).closest('tr')[0].childNodes[0].innerHTML;
            var _url = XNAT.url.restUrl('/xapi/metrics/reports/' + _id);

            XNAT.xhr.getJSON(_url).done(function(data){
                var _source = spawn('textarea', JSON.stringify(data, null, 4));

                var _editor = XNAT.app.codeEditor.init(_source, {
                    language: 'json'
                });

                _editor.openEditor({
                    title: 'Report',
                    classes: 'plugin-json',
                    footerContent: '(read-only)',
                    buttons: {
                        close: { label: 'Close' }
                    },
                    afterShow: function(dialog, obj){
                        obj.aceEditor.setReadOnly(true);
                    }
                })
            });
        });

        // Report generate btn
        function reportInit() {
            var activeGenerate = spawn('button.generate-report.btn1.submit', {
                html: 'Generate Report',
                style: {
                    marginTop: '16px',
                    float: 'right'
                },
                onclick: function(){
                    XNAT.xhr.post({
                        type: "POST",
                        url: XNAT.url.csrfUrl("/xapi/metrics/reports/generate"),
                        dataType: "json",
                        success: function() {
                            XNAT.ui.banner.top(3000, 'Report is generated successfully', 'success');
                            refreshTable();
                        },
                        error: function() {
                            console.log("ERROR in Generate Report");
                        },
                    });
                }
            });

            $manager.append(spawn('div', [
                activeGenerate,
                ['div.clear.clearfix']
            ]));

            return {
                element: $manager[0],
                spawned: $manager[0],
                get: function(){
                    return $manager[0]
                }
            };
        };

        // Refresh table
        function refreshTable() {
            $('div#report-table-container').empty();
            var newReportTable = XNAT.table.dataTable([], {
                id: 'reportTable',
                load: '/xapi/metrics/reports',
                columns: {
                    id: {
                        label: 'ID',
                        filter: true,
                        className: 'center'
                    },
                    created: {
                        label: 'Created',
                        filter: true,
                        apply: function(){
                            var item = this;
                            var date = new Date(item.created)
                            var reportDate = date.toLocaleString();
                            return spawn('div.center', reportDate + '');
                        },
                    },
                    status: {
                        label: 'Status',
                        filter: true,
                        className: 'center'
                    },
                    statusText: {
                        label: 'Status Text',
                        filter: true,
                        className: 'center'
                    },
                    attempts: {
                        label: 'Attempts',
                        filter: true,
                        className: 'center'
                    },
                    text: {
                        label: 'Text',
                        value: "&ndash;",
                        className: 'center',
                        html: '<a href="#!" class="view-report-info link nowrap">View JSON</a>'
                    },
                }
            }).render('div#report-table-container');

            $manager.prepend(spawn('div', [
                newReportTable
                ['div.clear.clearfix']
            ]));
        };

        // Calling the functions
        metaDataFunction();
        scheduleFunction();
        reportInit();
    })
)
